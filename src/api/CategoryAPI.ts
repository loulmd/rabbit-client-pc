import type { Pagination, XtxResponse } from "@/types/Response";
import XtxRequestManager from "@/utils/XtxRequestManager";
import type { Category } from "@/types/Category";
import type { Goods, GoodsRequestParams } from "@/types/Goods";

export class CategoryAPI {
  static getCategories() {
    return XtxRequestManager.instance.request<XtxResponse<Category[]>>({
      url: "/home/category/head",
    });
  }
  // 根据一级分类id获取一级分类的具体信息
  static getTopCategoryById(id: string) {
    return XtxRequestManager.instance.request<XtxResponse<Category>>({
      url: "/category",
      params: { id },
    });
  }
  // 根据二级分类 id 获取该分类下的商品的筛选条件
  static getSubCategoryFilters(id: string) {
    return XtxRequestManager.instance.request<XtxResponse<Category>>({
      url: "/category/sub/filter",
      params: { id },
    })
  }
  // 获取二级分类商品列表
  static getCategoryGoods(
    categoryId: GoodsRequestParams["categoryId"],
    reqParams?: Partial<Omit<GoodsRequestParams, "categoryId">>) {
    return XtxRequestManager.instance.request<
      XtxResponse<Pagination<Goods>>,
      Partial<GoodsRequestParams>>
      ({
        url: "/category/goods",
        method: "post",
        data: { categoryId, ...reqParams },
      })
  }

}
