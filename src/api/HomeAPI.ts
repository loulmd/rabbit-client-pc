import type { HotRecommends } from "./../types/HotRecommends";
import type { XtxResponse } from "@/types/Response";
import XtxRequestManager from "@/utils/XtxRequestManager";
import type { Brands } from "@/types/Brands";
import type { Banner } from "@/types/Banner";
import type { Goods } from "@/types/Goods";
import type { Category } from "@/types/Category";
import type { Special } from "@/types/Special";

export class HomeAPI {
  // 获取推荐的品牌数据
  static getHotBrands(limit: number) {
    return XtxRequestManager.instance.request<XtxResponse<Brands[]>>({
      url: "/home/brand",
      params: { limit },
    });
  }
  // 获取轮播图数据
  static getBanners(distributionSite: 1 | 2) {
    return XtxRequestManager.instance.request<XtxResponse<Banner[]>>({
      url: "/home/banner",
      params: { distributionSite },
    });
  }
  // 获取新鲜好物数据
  static getFreshGoods(limit: number) {
    return XtxRequestManager.instance.request<XtxResponse<Goods[]>>({
      url: "/home/new",
      params: { limit },
    });
  }
  // 获取人气推荐
  static getHotRecommends() {
    return XtxRequestManager.instance.request<XtxResponse<HotRecommends[]>>({
      url: "/home/hot",
    });
  }
  // 获取产品区块数据
  static getGoods() {
    return XtxRequestManager.instance.request<XtxResponse<Category[]>>({
      url: "/home/goods",
    });
  }
  // 获取最新专题
  static getSpecial(limit: number) {
    return XtxRequestManager.instance.request<XtxResponse<Special[]>>({
      url: "/home/special",
      params: { limit },
    });
  }
}
