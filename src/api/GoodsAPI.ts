import type { Goods } from "@/types/Goods";
import type { XtxResponse } from "@/types/Response";
import XtxRequestManager from "@/utils/XtxRequestManager";

export class GoodsAPI {
    // 根据商品 id 获取商品的详细信息
    static getGoodsInfo(id: string) {
        return XtxRequestManager.instance.request<XtxResponse<Goods>
        >({
            url: "/goods",
            params: { id },
        });
    }
    // 获取同类商品
    // id : 商品id 传递了表示同类推荐 没传表示猜你喜欢
    // limit: 限制请求的商品数量
    static getRelevantGoods(args?: { id?: string; limit?: number }) {
        // 如果传递了 arg 都是没有传递 limit
        if (typeof args !== "undefined" && typeof args.limit === "undefined") {
            args.limit = 16;
        } else {
            // 如果没有传递 arg
            args = { limit: 16 };
        }
        return XtxRequestManager.instance.request<XtxResponse<Goods[]>>({
            url: "/goods/relevant",
            params: args,
        })
    }
    // 获取热销榜单
    static getHotSaleGoods(id: string, type: 1 | 2 | 3, limit: number = 3) {
        return XtxRequestManager.instance.request<XtxResponse<Goods[]>>({
            url: "goods/hot",
            params: { id, type, limit }
        })
    }
}