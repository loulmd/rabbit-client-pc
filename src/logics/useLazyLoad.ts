// 监听元素是否进入可视区, 如果进入发送请求获取数据
type APIFunction = () => Promise<void>;

export default function useLazyLoad(apiFunction: APIFunction) {
  const target = ref<HTMLElement>();
  // 监听元素是否进入可视区
  const { stop } = useIntersectionObserver(
    target,
    ([{ isIntersecting }]) => {
      if (isIntersecting) {
        // 停止监听2
        stop();
        // 发送请求获取数据
        apiFunction();
      }
    },
    { threshold: 0 }
  );
  // 将 target 返回, 方法的调用者负责绑定元素, 他绑定谁我们就监听谁
  return target;
}
