export default function useScrollTop() {
  // 用于存储滚动距离的状态
  const scrollTop = ref(0);

  // 滚动事件的事件处理函数
  const scrollHandler = () => {
    // 当滚动事件触发以后, 获取滚动距离, 存储滚动距离
    scrollTop.value = document.documentElement.scrollTop;
  };

  // 组件挂载完成之后
  onMounted(() => {
    // 为 window 添加滚动事件
    window.addEventListener("scroll", scrollHandler);
  });
  // 组件卸载之后
  onUnmounted(() => {
    // 清除 window 对象的滚动事件
    window.removeEventListener("scroll", scrollHandler);
  });

  // 返回滚动距离供外部获取
  return scrollTop;
}
