import type { User } from "@/types/User";
import { defineStore } from "pinia";

type States = {
  profile: Partial<User>;
};

type Getters = {};
type Actions = {};

export const useUserStore = defineStore<"user", States, Getters, Actions>(
  "user",
  {
    state: () => ({ profile: {} }),
    persist: true,
  }
);
