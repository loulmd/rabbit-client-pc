import { GoodsAPI } from "@/api/GoodsAPI";
import type { Goods, GoodsDetailInfo } from "@/types/Goods";
import type { Status } from "@/types/Status";
import { defineStore } from "pinia"
import chunk from "lodash/chunk";

// 声明组件外部传递的状态的类型规范
export interface Data {
    price: string;
    oldPrice: string;
    inventory: number;
    skuId: string;
}


type State = {
    // 商品信息
    goodsInfo: {
        status: Status;
        result: Goods;
    };
    // 同类商品和 猜你喜欢
    relevantGoods: {
        // 猜你喜欢
        result: Goods[][];
        status: Status;
    }
    // 榜单
    hotSaleGoods: {
        // 加载状态
        status: Status;
        // 榜单数据
        result: {
            // 24小时
            1: Goods[];
            // 周榜
            2: Goods[];
            // 总榜
            3: Goods[];
        }
    }
};


type Getters = {
    mainPictures(): string[];
    baseInfo(): Pick<Goods, "name" | "desc" | "price" | "oldPrice">
    // 获取商品详细信息
    goodsProperties(): GoodsDetailInfo;
};

type Actions = {
    // 根据商品id获取商品信息
    getGoodsInfo(id: string): Promise<void>;
    // 更新商品信息 
    updateGoods(data: Data): void;
    // 获取腾讯商品 和 猜你喜欢
    getRelevantGoods(args?: { id?: string; limit?: number }): Promise<void>;
    // 获取榜单
    getHotSaleGoods(id: string, type: 1 | 2 | 3, limit: number): Promise<void>;
};


export const useGoodsStore = defineStore<"goods", State, Getters, Actions>(
    "goods",
    {
        state: () => ({
            // 商品信息
            goodsInfo: {
                result: {
                    id: "",
                    name: "",
                    desc: "",
                    price: "",
                    picture: "",
                    discount: null,
                    orderNum: null,
                    spuCode: "",
                    oldPrice: "",
                    inventory: 0,
                    brand: [],
                    salesCount: 0,
                    commentCount: 0,
                    collectCount: 0,
                    mainVideos: [],
                    videoScale: 0,
                    mainPictures: [],
                    specs: [],
                    skus: [],
                    categories: [],
                    details: {
                        pictures: [],
                        properties: [],
                    },
                    isPreSale: false,
                    isCollect: false,
                    userAddresses: null,
                    similarProducts: [],
                    hotByDay: [],
                    evaluationInfo: null,
                },
                status: "idle",
            },
            // 同类商品和猜你喜欢
            relevantGoods: {
                // 加载状态
                status: "idle",
                // 同类商品
                result: [],
            },
            // 榜单
            hotSaleGoods: {
                status: "idle",
                result: {
                    1: [],
                    2: [],
                    3: []
                },
            },
        }),
        getters: {
            mainPictures() {
                return this.goodsInfo.result.mainPictures
            },
            baseInfo() {
                const { name, desc, price, oldPrice } = this.goodsInfo.result;
                return { name, desc, price, oldPrice }
            },
            // 获取商品详细信息
            goodsProperties() {
                return {
                    // 商品详细图片集合
                    pictures: this.goodsInfo.result.details.pictures,
                    // 商品属性集合
                    properties: this.goodsInfo.result.details.properties,
                }
            }
        },
        actions: {
            // 根据商品id获取商品信息
            async getGoodsInfo(id) {
                // 更新加载状态
                this.goodsInfo.status = "loading";
                // 捕获错误
                try {
                    // 发送请求, 根据商品 id 获取商品详细信息
                    // GoodsAPI.getGoodsInfo(id);
                    const response = await GoodsAPI.getGoodsInfo(id);
                    // 存储商品详细信息
                    this.goodsInfo.result = response.result;
                    // 更新加载状态
                    this.goodsInfo.status = "success";
                } catch (error) {
                    // 更新加载状态
                    this.goodsInfo.status = "error";
                }
            },
            // 更新商品信息
            updateGoods(data) {
                this.goodsInfo.result.price = data.price;
                this.goodsInfo.result.oldPrice = data.oldPrice;
                this.goodsInfo.result.inventory = data.inventory;
            },
            // 获取同类商品
            async getRelevantGoods(args) {
                // 更新加载状态
                this.relevantGoods.status = "loading";
                // 捕获错误
                try {
                    // 发送请求获取同类商品
                    const response = await GoodsAPI.getRelevantGoods(args)
                    // 为商品进行分组 4个商品一组
                    this.relevantGoods.result = chunk(response.result, 4)
                    // 更新加载状态
                    this.relevantGoods.status = "success";
                } catch (error) {
                    // 更新加载状态
                    this.relevantGoods.status = "error"
                }
            },
            // 获取榜单
            async getHotSaleGoods(id, type, limit) {
                // 更新加载状态
                this.hotSaleGoods.status = "loading";
                // 捕获错误
                try {
                    // 发送请求获取榜单商品
                    const response = await GoodsAPI.getHotSaleGoods(id, type, limit);
                    // 按照榜单类别存储榜单商品
                    this.hotSaleGoods.result[type] = response.result;
                    // 更新加载状态
                    this.hotSaleGoods.status = "success";
                } catch (error) {
                    // 更新加载状态
                    this.hotSaleGoods.status = "error"
                }
            }
        },
    }
);