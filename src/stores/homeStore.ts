import type { Special } from "@/types/Special";
import { HomeAPI } from "@/api/HomeAPI";
import type { Brands } from "@/types/Brands";
import type { Status } from "@/types/Status";
import { defineStore } from "pinia";
import type { Banner } from "@/types/Banner";
import type { Goods } from "@/types/Goods";
import type { HotRecommends } from "@/types/HotRecommends";
import type { Category } from "@/types/Category";

type States = {
  // 推荐的品牌数据
  brands: {
    result: Brands[];
    status: Status;
  };
  // 轮播图数据
  banners: {
    result: Banner[];
    status: Status;
  };
  // 新鲜好物
  freshGoods: {
    result: Goods[];
    status: Status;
  };
  // 人气推荐
  hotRecommends: {
    result: HotRecommends[];
    status: Status;
  };
  // 产品数据
  goods: {
    result: Category[];
    status: Status;
  };
  // 最新专题
  special: {
    result: Special[];
    status: Status;
  };
};
type Getters = {};
type Actions = {
  // 获取推荐的品牌数据
  getBrands(limit?: number): Promise<void>;
  // 获取轮播图数据
  getBanners(distributionSite?: 1 | 2): Promise<void>;
  // 获取新鲜好物数据
  getFreshGoods(limit?: number): Promise<void>;
  // 获取人气推荐数据
  getHotRecommends(): Promise<void>;
  // 获取产品数据
  getGoods(): Promise<void>;
  // 获取最新专题数据
  getSpecial(limit?: number): Promise<void>;
};

export const useHomeStore = defineStore<"home", States, Getters, Actions>(
  "home",
  {
    state: () => ({
      // 推荐的品牌数据
      brands: {
        result: [],
        status: "idle",
      },
      // 轮播图数据
      banners: {
        result: [],
        status: "idle",
      },
      // 新鲜好物
      freshGoods: {
        result: [],
        status: "idle",
      },
      // 人气推荐
      hotRecommends: {
        result: [],
        status: "idle",
      },
      // 产品区块数据
      goods: {
        result: [],
        status: "idle",
      },
      // 最新专题
      special: {
        result: [],
        status: "idle",
      },
    }),
    actions: {
      // 获取推荐品牌数据
      async getBrands(limit = 9) {
        // 更新请求状态
        this.brands.status = "loading";
        // 捕获错误
        try {
          // 发送请求 获取推荐的品牌数据
          const response = await HomeAPI.getHotBrands(limit);
          // 存储推荐的品牌数据
          this.brands.result = response.result;
          // 更新请求状态
          this.brands.status = "success";
        } catch (error) {
          // 更新请求状态
          this.brands.status = "error";
        }
      },
      // 获取轮播图数据
      async getBanners(distributionSite = 1) {
        // 更新请求状态
        this.banners.status = "loading";
        // 捕获错误
        try {
          // 发送请求 获取轮播图数据
          const response = await HomeAPI.getBanners(distributionSite);
          // 保存轮播图数据
          this.banners.result = response.result;
          // 更新请求状态
          this.banners.status = "success";
        } catch (error) {
          // 更新请求状态
          this.banners.status = "error";
        }
      },
      // 获取新鲜好物数据
      async getFreshGoods(limit = 4) {
        // 更新加载状态
        this.freshGoods.status = "loading";
        // 捕获错误
        try {
          // 发送请求 获取新鲜好物数据
          const response = await HomeAPI.getFreshGoods(limit);
          // 存储数据
          this.freshGoods.result = response.result;
          // 更新加载状态
          this.freshGoods.status = "success";
        } catch (error) {
          // 更新加载状态
          this.freshGoods.status = "error";
        }
      },
      // 获取人气推荐
      async getHotRecommends() {
        // 更新加载状态
        this.hotRecommends.status = "loading";
        // 捕获错误
        try {
          // 发送请求 获取人气推荐数据
          const response = await HomeAPI.getHotRecommends();
          // 存储人气推荐
          this.hotRecommends.result = response.result;
          // 更新加载状态
          this.hotRecommends.status = "success";
        } catch (error) {
          // 更新加载状态
          this.hotRecommends.status = "error";
        }
      },
      // 获取产品区块数据
      async getGoods() {
        // 更新请求状态
        this.goods.status = "loading";
        // 捕获错误
        try {
          // 发送请求获取数据
          const response = await HomeAPI.getGoods();
          // 保存产品区块数据
          this.goods.result = response.result;
          // 更新请求状态
          this.goods.status = "success";
        } catch (error) {
          // 更新请求状态
          this.goods.status = "error";
        }
      },
      // 获取专题专题
      async getSpecial(limit = 3) {
        // 更新加载状态
        this.special.status = "loading";
        // 捕获错误
        try {
          // 发送请求获取数据
          const response = await HomeAPI.getSpecial(limit);
          // 存储最新专题数据
          this.special.result = response.result;
          // 更新加载状态
          this.special.status = "success";
        } catch (error) {
          // 更新加载状态
          this.special.status = "error";
        }
      },
    },
  }
);
